/* main.c - Application main entry point */

/*
 * Copyright (c) 2015-2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/types.h>
#include <stddef.h>
#include <errno.h>
#include <zephyr.h>
#include <sys/printk.h>

#include "buttons.h"
#include "i2c_bus.h"
#include "uart_bus.h"
#include "ble_mgr.h"

#include <logging/log.h>
LOG_MODULE_REGISTER(BlePeripheral, CONFIG_LOG_MAX_LEVEL); //required to enable LOG_DBG, LOG_ERR
#define sd(x) log_strdup((x))

void main(void) {
	k_sleep(K_SECONDS(2)); //delay to allow terminal to init and show printk statements

	printk("Begin initialization...\n");

	//setup the I2C bus
	int err = init_i2c_bus();
	if (err != 0) {
		printk("Error configuring i2c\n");
		return;
	} else {
		printk("I2C initialized...\n");
	}

	err = init_uart();
	if (err != 0) {
		printk("Error configuring UART\n");
		return;
	} else {
		printk("UART initialized...\n");
	}

	//TEST ONLY setup button handle reading I2C for BLE addresses
	err = init_buttons();
	if (err != 0) {
		printk("Error initializing buttons\n");
		return;
	} else {
		printk("Buttons initialized...\n");
	}

	//initialize BLE
	err = ble_init();
	if (err != 0) {
		printk("Error initializing BLE\n");
		return;
	} else {
		printk("\nBluetooth initialized...\n");
	}

	printk("Entering main loop...\n");
	while (1) {
		k_sleep(K_MSEC(100)); //TODO low power mode
		//k_cpu_idle();
	}
}
