#include "i2c_bus.h"

#define ARDUINO_I2C_ADDR                8
static const struct device              *m_i2c_dev;

/**@brief Initialize the I2C Bus
 * @return 0 on success
 */
int init_i2c_bus(void) {
    int ret = 0;
    //I2C_0 is connected to P0.26 and P0.27 on the nRF52-DK
    //connected to Ardino pins A5(P0.27) and A4(P0.26)
    m_i2c_dev = device_get_binding("I2C_0");
	if (!m_i2c_dev) {
		printk("I2C: Device driver not found\n");
        ret = -1;
	} else {
		uint32_t i2c_cfg = I2C_SPEED_SET(I2C_SPEED_STANDARD) | I2C_MODE_MASTER;
		if (i2c_configure(m_i2c_dev, i2c_cfg)) {
			printk("I2C config failed");
            ret = -1;
		}
	}
    return ret;
}

/**@brief Request via I2C
 * @details Write a request to the response that solicits a response
 * @param[in] write_buf data to write
 * @param[in] write_sz number of bytes to write
 * @param[out] read_buf response from device
 * @param[in] read_sz number of bytes in response
 * @return 0 on success
 */
int i2c_request(uint8_t *write_buf, size_t write_sz, uint8_t *read_buf, size_t read_sz) {
    int ret = i2c_write_read(m_i2c_dev, ARDUINO_I2C_ADDR, write_buf, write_sz, read_buf, read_sz);
    if (ret) {
        printk("Error writing to i2c %d", ret);
    } else {
        printk("Read: %s\n", read_buf);
    }
    return ret;
}