#include "uart_bus.h"

#include <drivers/uart.h>
#include <devicetree.h>

#define UART_DEVICE_NAME "UART_0"
static const struct device      *m_uart_dev;
static volatile bool m_data_received;
static uint8_t uart_buf[128];
static int uart_index = 0;

static void uart_cb(const struct device *dev, void *user_data) {
    uart_irq_update(dev);
    if (uart_irq_rx_ready(dev)) {
        uint8_t tmp[10];
        int len = uart_fifo_read(dev, tmp, sizeof(tmp));
        if (len == 1) {
            uart_buf[uart_index++] = tmp[0];
            //newline indicates end of UART msg
            if (tmp[0] == '\n') {
                printk("Got data: %s (%d)", uart_buf, uart_index);
                if (uart_index == 3) {
                    printk("Sending 1\n");
                    uart_poll_out(dev, '1');
                } else {
                    printk("Sending 0\n");
                    uart_poll_out(dev, '0');
                }
                uart_index = 0;
            }
        } else {
            printk("Data > 1\n");
        }
    }
}

int init_uart(void) {
    const struct uart_config cfg = {
        .baudrate = 115200,
        .parity = UART_CFG_PARITY_NONE,
        .stop_bits = UART_CFG_STOP_BITS_1,
        .data_bits = UART_CFG_DATA_BITS_8,
        .flow_ctrl = UART_CFG_FLOW_CTRL_NONE
    };

    m_uart_dev = device_get_binding(UART_DEVICE_NAME);
    if (m_uart_dev == NULL) {
        printk("Error getting UART binding\n");
        return -1;
    }

    int err = uart_configure(m_uart_dev, &cfg);
    if (!err) {
        uart_irq_callback_set(m_uart_dev, uart_cb);
        uart_irq_rx_enable(m_uart_dev);
    }

    return err;
}