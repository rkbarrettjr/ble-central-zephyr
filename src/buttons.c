#include "buttons.h"

#include <drivers/gpio.h>
#include <string.h>
#include "i2c_bus.h"
#include "ble_mgr.h"
#include "uart_bus.h"

//configure buttons to simulate NFC read
#define SW0_NODE 						DT_ALIAS(sw0)
#if DT_NODE_HAS_STATUS(SW0_NODE, okay)
#define SW0_GPIO_LABEL 					DT_GPIO_LABEL(SW0_NODE, gpios)
#define SW0_GPIO_PIN					DT_GPIO_PIN(SW0_NODE, gpios)
#define SW0_GPIO_FLAGS					(GPIO_INPUT | DT_GPIO_FLAGS(SW0_NODE, gpios))
#else
#define SW0_GPIO_LABEL 					''
#define SW0_GPIO_PIN					0
#define SW0_GPIO_FLAGS					0
#endif

#define SW1_NODE 						DT_ALIAS(sw1)
#if DT_NODE_HAS_STATUS(SW1_NODE, okay)
#define SW1_GPIO_LABEL 					DT_GPIO_LABEL(SW1_NODE, gpios)
#define SW1_GPIO_PIN					DT_GPIO_PIN(SW1_NODE, gpios)
#define SW1_GPIO_FLAGS					(GPIO_INPUT | DT_GPIO_FLAGS(SW1_NODE, gpios))
#else
#define SW1_GPIO_LABEL 					''
#define SW1_GPIO_PIN					0
#define SW1_GPIO_FLAGS					0
#endif

#define SW2_NODE 						DT_ALIAS(sw2)
#if DT_NODE_HAS_STATUS(SW2_NODE, okay)
#define SW2_GPIO_LABEL 					DT_GPIO_LABEL(SW2_NODE, gpios)
#define SW2_GPIO_PIN					DT_GPIO_PIN(SW2_NODE, gpios)
#define SW2_GPIO_FLAGS					(GPIO_INPUT | DT_GPIO_FLAGS(SW2_NODE, gpios))
#else
#define SW3_GPIO_LABEL 					''
#define SW3_GPIO_PIN					0
#define SW3_GPIO_FLAGS					0
#endif

static struct gpio_callback button_cb_data1;
static struct gpio_callback button_cb_data2;
static struct gpio_callback button_cb_data3;

#define MY_STACK_SIZE 512
#define MY_PRIORITY 5

// container for function operating in worker thread to handle button presses
struct button_info {
    struct k_work work;
    char name[16];
    uint8_t button_pressed;
} my_button;

//setup workqueue to read I2C when button is pressed
K_THREAD_STACK_DEFINE(my_stack_area, MY_STACK_SIZE);
static struct k_work_q my_work_q;

//local helper functions
static int configure_buttons(void);
static void button_press_worker(struct k_work *item);

//initalize buttons
int init_buttons(void) {
    //setup workqueue to offload button interrupts
    k_work_q_start(&my_work_q, my_stack_area, MY_STACK_SIZE, MY_PRIORITY);
    //assign callback to perform work
    k_work_init(&my_button.work, button_press_worker);
    //setup the buttons to simulate interrupts coming from the NFC chip
    return configure_buttons();
}

//function running on worker thread to read I2C and create BLE connection to read address
static void button_press_worker(struct k_work *item) {
    uint8_t write_buf[1];
    struct button_info *button = CONTAINER_OF(item, struct button_info, work);
    if (button->button_pressed == 2) {
        printk("Button 3 press\n");
    } else {
        if (button->button_pressed == 0) {
            write_buf[0] = '0';
        } else {
            write_buf[0] = '1';
        }
        printk("%s pressed\n", button->name);

        uint8_t advertised_name[DEVICE_NAME_LEN];
        //read the I2C for the BLE device name to connect
        i2c_request(write_buf, 1, advertised_name, sizeof(advertised_name));
        int err = start_scan(advertised_name);
        if (err != 0) {
            printk("Could not create connection\n");
        }
    }
}

//callback for button 1 press
void button1_pressed_cb(const struct device *dev, struct gpio_callback *cb,
            uint32_t pins) {
    memcpy(my_button.name, "Button 1", 8);
    my_button.button_pressed = 0;
    //offload to lower priority thread
    k_work_submit(&my_button.work);
}

//callback for button 2 press
void button2_pressed_cb(const struct device *dev, struct gpio_callback *cb,
            uint32_t pins) {
    memcpy(my_button.name, "Button 2", 8);
    my_button.button_pressed = 1;
    //offload to lower priority thread
    k_work_submit(&my_button.work);
}

//callback for button 3 press
void button3_pressed_cb(const struct device *dev, struct gpio_callback *cb,
            uint32_t pins) {
    memcpy(my_button.name, "Button 3", 8);
    my_button.button_pressed = 2;
    //offload to lower priority thread
    k_work_submit(&my_button.work);
}

//helper to setup 3 buttons
static int configure_buttons(void) {
    int ret = -1;
    const struct device *button = device_get_binding(SW0_GPIO_LABEL);
	if (button != NULL) {
        ret = gpio_pin_configure(button, SW0_GPIO_PIN, SW0_GPIO_FLAGS);
        if (ret == 0) {
            ret = gpio_pin_interrupt_configure(button, SW0_GPIO_PIN, GPIO_INT_EDGE_TO_ACTIVE);
            if (ret == 0) {
                gpio_init_callback(&button_cb_data1, button1_pressed_cb, BIT(SW0_GPIO_PIN));
	            gpio_add_callback(button, &button_cb_data1);
            }
        }
	}
    if (ret == 0) {
        button = device_get_binding(SW1_GPIO_LABEL);
        if (button != NULL) {
            int ret = gpio_pin_configure(button, SW1_GPIO_PIN, SW1_GPIO_FLAGS);
            if (ret == 0) {
                ret = gpio_pin_interrupt_configure(button, SW1_GPIO_PIN, GPIO_INT_EDGE_TO_ACTIVE);
                if (ret == 0) {
                    gpio_init_callback(&button_cb_data2, button2_pressed_cb, BIT(SW1_GPIO_PIN));
                    gpio_add_callback(button, &button_cb_data2);
                }
            }
        }
    }
    if (ret == 0) {
        button = device_get_binding(SW2_GPIO_LABEL);
        if (button != NULL) {
            int ret = gpio_pin_configure(button, SW2_GPIO_PIN, SW2_GPIO_FLAGS);
            if (ret == 0) {
                ret = gpio_pin_interrupt_configure(button, SW2_GPIO_PIN, GPIO_INT_EDGE_TO_ACTIVE);
                if (ret == 0) {
                    gpio_init_callback(&button_cb_data3, button3_pressed_cb, BIT(SW2_GPIO_PIN));
                    gpio_add_callback(button, &button_cb_data3);
                }
            }
        }
    }
    return ret;
}