#include "ble_mgr.h"

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>

//service UUID
#define 						TIDI_SVC_UUID   0x1C, 0xF7, 0x8F, 0x1B, 0x84, 0xBC, 0x85, 0x98, 0x68, 0x49, 0xDA, 0x9E, 0x00, 0x20, 0x0F, 0xF1
//control characteristic
#define 						TIDI_CTRL_UUID  0x1C, 0xF7, 0x8F, 0x1B, 0x84, 0xBC, 0x85, 0x98, 0x68, 0x49, 0xDA, 0x9E, 0x01, 0x20, 0x0F, 0xF1
//info characteristic
#define 						TIDI_INFO_UUID  0x1C, 0xF7, 0x8F, 0x1B, 0x84, 0xBC, 0x85, 0x98, 0x68, 0x49, 0xDA, 0x9E, 0x02, 0x20, 0x0F, 0xF1
#define							MAX_BLE_CONNECTIONS  	2

static struct bt_conn 					*m_establishing_connection;						/**< placeholder for connection while it is establihing */
static struct bt_uuid_16 				m_uuid16 = BT_UUID_INIT_16(0);					/**< 16 bit UUID */
static struct bt_uuid_128 				m_uuid128 = BT_UUID_INIT_128(0);				/**< 128 bit UUID */
static struct bt_gatt_discover_params 	m_discover_params;								/**< holder for UUID discover params */
static struct bt_gatt_subscribe_params 	m_subscribe_params;								/**< holder for UUID subscribe params */
static struct bt_gatt_read_params 		m_read_params;									/**< holder for UUID reading */
static struct bt_gatt_write_params 		m_write_params;									/**< holder for UUID writing */
static uint8_t 							m_num_connected_devs = 0;						/**< tracks number of connected devices */
static uint16_t 						m_handle;										/**< temp storage for write then read of a UUID */
static char								m_scan_name[DEVICE_NAME_LEN + 1];
static char 							m_dev_addr[BT_ADDR_LE_STR_LEN];

//local helper functions
static int create_ble_connection(const char *addr);
static void connected(struct bt_conn *conn, uint8_t conn_err);
static void disconnected(struct bt_conn *conn, uint8_t reason) ;
static uint8_t notify_func(struct bt_conn *conn,struct bt_gatt_subscribe_params *params,
			   const void *data, uint16_t length);
static uint8_t read_cb(struct bt_conn *conn, uint8_t err, struct bt_gatt_read_params *params, 
                const void *data, uint16_t length);
static void write_cb(struct bt_conn *conn, uint8_t err, struct bt_gatt_write_params *params);
static uint8_t discover_func(struct bt_conn *conn, const struct bt_gatt_attr *attr, 
				struct bt_gatt_discover_params *params);static void device_found(const bt_addr_le_t *addr, int8_t rssi, uint8_t type,
			 struct net_buf_simple *ad);

static struct bt_conn_cb conn_callbacks = {
	.connected = connected,
	.disconnected = disconnected,
};

/**@brief BLE Initialization
 * @return 0 on success
 */
int ble_init(void) {
	int err = bt_enable(NULL);
	if (err == 0) {
		bt_conn_cb_register(&conn_callbacks);
	}
	return err;
}

//begin scan for BLE devices
int start_scan(char *advertised_name) {
	int err;

	//stop scanning once max number devices reached
	if (m_num_connected_devs >= MAX_BLE_CONNECTIONS) {
		return -1;
	}

	//save the advertised name we are scanning for
	memcpy(m_scan_name, advertised_name, 17);
	m_scan_name[16] = '\0';
	printk("Begin scanning for device: %s\n", m_scan_name);

	/* Use active scanning and disable duplicate filtering to handle any
	 * devices that might update their advertising data at runtime. */
	struct bt_le_scan_param scan_param = {
		.type       = BT_LE_SCAN_TYPE_ACTIVE,
		.options    = BT_LE_SCAN_OPT_FILTER_DUPLICATE, //TODO - this or NONE
		.interval   = BT_GAP_SCAN_FAST_INTERVAL,
		.window     = BT_GAP_SCAN_FAST_WINDOW,
	};


	err = bt_le_scan_start(&scan_param, device_found);
	if (err) {
		printk("Scanning failed to start (err %d)\n", err);
	} else {
		printk("Scanning successfully started\n");
	}
	return err;
}

/**@brief Create BLE Connection
 * @param[in] target Bluetooth address <00:11:22:33:44:55>
 * @return 0 on success
 */
static int create_ble_connection(const char *addr) {
	if (m_num_connected_devs >= MAX_BLE_CONNECTIONS) {
		return -1;
	}
	bt_addr_le_t bleAddr;
	int err = bt_addr_le_from_str(addr, "random", &bleAddr);
	if (err != 0) {
		printk("Error (%d) creating bt_addr_le_t: %s\n", err, addr);
	} else {
		struct bt_le_conn_param *param = BT_LE_CONN_PARAM_DEFAULT;

		err = bt_conn_le_create(&bleAddr, BT_CONN_LE_CREATE_CONN,
			param, &m_establishing_connection);
		if (err != 0) {
			printk("Error (%d) connecting: %s", err, addr);
		}
	}
	return err;
}

static bool eir_found(struct bt_data *data, void *user_data) {
	if (data->type == BT_DATA_NAME_COMPLETE) {
		printk("Got a complete name\n");
		char name[DEVICE_NAME_LEN];
		memcpy(name, data->data, sizeof(name));
		if (!memcmp(m_scan_name, name, DEVICE_NAME_LEN)) {
			printk("Found Device: %s\n", m_scan_name);
			int err = bt_le_scan_stop();
			if (err) {
				printk("Stop LE scan failed (err %d)\n", err);
			}
			create_ble_connection(m_dev_addr);
		}
	}
	printk("data type: %d\n", data->type);

	return true;
}

//call back from BLE Scan when device is found
static void device_found(const bt_addr_le_t *addr, int8_t rssi, uint8_t type,
			 struct net_buf_simple *ad) {

	bt_addr_le_to_str(addr, m_dev_addr, 18);

	//only interested in connectable events
	if (type == BT_GAP_ADV_TYPE_ADV_IND ||
	    type == BT_GAP_ADV_TYPE_ADV_DIRECT_IND) {
		printk("[DEVICE]: %s, AD evt type %u, AD data len %u, RSSI %i\n",
	       	m_dev_addr, type, ad->len, rssi);
		
		//perform Extended Inquiry on device
		bt_data_parse(ad, eir_found, (void *)addr);
	}
}

//callback when device makes a connection
static void connected(struct bt_conn *conn, uint8_t conn_err) {
	char addr[BT_ADDR_LE_STR_LEN];
	int err;

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

	if (conn_err) {
		printk("Failed to connect to %s (%u)\n", addr, conn_err);
		bt_conn_unref(conn);
		return;
	}

	m_num_connected_devs++;
	printk("Connected: %s (Total devices connected: %d)\n", addr, m_num_connected_devs);
	
	if (conn == m_establishing_connection) {
		//look for the TIDI service
		memcpy(&m_uuid128, BT_UUID_DECLARE_128(TIDI_SVC_UUID), sizeof(m_uuid128));
		m_discover_params.uuid = &m_uuid128.uuid;
		m_discover_params.func = discover_func;
		m_discover_params.start_handle = 0x0001;
		m_discover_params.end_handle = 0xffff;
		m_discover_params.type = BT_GATT_DISCOVER_PRIMARY;

		//discovered devices will be handled in the discover_func() callback
		err = bt_gatt_discover(conn, &m_discover_params);
		if (err) {
			printk("Immediate discover failed(err %d)\n", err);
			return;
		}
	}
}

//callback when device disconnects
static void disconnected(struct bt_conn *conn, uint8_t reason) {
	char addr[BT_ADDR_LE_STR_LEN];

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

	m_num_connected_devs--;
	printk("Disconnected: %s (reason 0x%02x) (Total devices connected: %d)\n", addr, reason, m_num_connected_devs);

	bt_conn_unref(conn);
}

//callback function for notifications
static uint8_t notify_func(struct bt_conn *conn,
			   struct bt_gatt_subscribe_params *params,
			   const void *data, uint16_t length) {
	if (!data) {
		printk("[UNSUBSCRIBED]\n");
		params->value_handle = 0U;
		return BT_GATT_ITER_STOP;
	}

	const bt_addr_le_t *ble_addr = bt_conn_get_dst(conn);
	char dest_addr[30];
	bt_addr_le_to_str(ble_addr, dest_addr, sizeof(dest_addr));

	//display the data in the notification
	uint8_t *tmp = (uint8_t *)data;
	printk("[NOTIFICATION-%s] Data: ", dest_addr);
	for (int i = 0; i < length; i++) {
		printk("%02x ", tmp[i]);
	}
	printk("\n");

	return BT_GATT_ITER_CONTINUE;
}

//callback for reading
static uint8_t read_cb(struct bt_conn *conn, uint8_t err, struct bt_gatt_read_params *params, const void *data, uint16_t length) {
	uint8_t *tmp = (uint8_t *)data;
	printk("Read Data: ");
	for (int i = 0; i < length; i++) {
		printk("%x ", tmp[i]);
	}
	printk("\n");
	return 0;
}

//callback for writing
static void write_cb(struct bt_conn *conn, uint8_t err, struct bt_gatt_write_params *params) {
	printk("Got write callback: %d\n", err);
	//setup to read back data written
	m_read_params.func = read_cb;
	m_read_params.handle_count = 1;
	m_read_params.single.handle = m_handle;
	m_read_params.single.offset = 0;
	bt_gatt_read(conn, &m_read_params);
}

//call back function when services, characteristics, and descriptors are discovered
static uint8_t discover_func(struct bt_conn *conn, const struct bt_gatt_attr *attr, 
								struct bt_gatt_discover_params *params) {
	int err;
	if (!attr) {
		printk("Discover attr is NULL\n");
		(void)memset(params, 0, sizeof(*params));
		return BT_GATT_ITER_STOP;
	}
	printk("[ATTRIBUTE] handle %u\n", attr->handle);

	//discover services, characteristics, and descriptors
	if (!bt_uuid_cmp(m_discover_params.uuid, BT_UUID_DECLARE_128(TIDI_SVC_UUID))) {
		printk("Discovered the TIDI Service\n");
		//discover the Control characteristic next
		memcpy(&m_uuid128, BT_UUID_DECLARE_128(TIDI_CTRL_UUID), sizeof(m_uuid128));
		m_discover_params.uuid = &m_uuid128.uuid;
		m_discover_params.type = BT_GATT_DISCOVER_CHARACTERISTIC;
		m_discover_params.start_handle = attr->handle + 1;
		err = bt_gatt_discover(conn, &m_discover_params);
		if (err) {
			printk("Discover failed (err %d)\n", err);
		}
	} else if (!bt_uuid_cmp(m_discover_params.uuid, BT_UUID_DECLARE_128(TIDI_CTRL_UUID))) {
		printk("Discovered the TIDI Control Characteristic\n");
		//discover the Info chacteristic next
		memcpy(&m_uuid128, BT_UUID_DECLARE_128(TIDI_INFO_UUID), sizeof(m_uuid128));
		m_discover_params.uuid = &m_uuid128.uuid;
		m_discover_params.type = BT_GATT_DISCOVER_CHARACTERISTIC;
		m_discover_params.start_handle = attr->handle + 1;
		err = bt_gatt_discover(conn, &m_discover_params);
		if (err) {
			printk("Discover failed (err %d)\n", err);
		}

		//write to the UUID characteristic
		uint32_t data = 0x42424242;
		m_write_params.data = &data;
		m_write_params.length = 4;
		m_write_params.handle = attr->handle+1;
		m_handle = attr->handle+1;
		m_write_params.offset = 0;
		m_write_params.func = write_cb;
		bt_gatt_write(conn, &m_write_params);
	} else if (!bt_uuid_cmp(m_discover_params.uuid, BT_UUID_DECLARE_128(TIDI_INFO_UUID))) {
		printk("Discovered the TIDI Info Characteristic\n");
		//discover the CCC descriptor next
		memcpy(&m_uuid16, BT_UUID_GATT_CCC, sizeof(m_uuid16));
		m_discover_params.uuid = &m_uuid16.uuid;
		m_discover_params.type = BT_GATT_DISCOVER_DESCRIPTOR;
		m_discover_params.start_handle = attr->handle + 1;
		//save the handle of the Info characteristic attribute
		m_subscribe_params.value_handle = bt_gatt_attr_value_handle(attr);
		err = bt_gatt_discover(conn, &m_discover_params);
		if (err) {
			printk("Discover failed (err %d)\n", err);
		}
	} else {
		printk("Discover GATT CCC Descriptor\n");
		//subscribe to the notification
		m_subscribe_params.notify = notify_func;
		m_subscribe_params.value = BT_GATT_CCC_NOTIFY;
		m_subscribe_params.ccc_handle = attr->handle;

		err = bt_gatt_subscribe(conn, &m_subscribe_params);
		if (err && err != -EALREADY) {
			printk("Subscribe failed (err %d)\n", err);
		} else {
			printk("[SUBSCRIBED]\n");
		}
	}
	return BT_GATT_ITER_STOP;
}