#ifndef I2C_BUS_H
#define I2C_BUS_H

#include <stdint.h>
#include <drivers/i2c.h>

int init_i2c_bus(void);
int i2c_request(uint8_t *write_buf, size_t write_sz, uint8_t *read_buf, size_t read_sz);

#endif