#ifndef BLE_MGR_H
#define BLE_MGR_H

#define DEVICE_NAME_LEN 16

int ble_init(void);
int start_scan(char *advertised_name);

#endif
